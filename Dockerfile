FROM debian:latest
MAINTAINER Denis Zadonskii <d.zadonsky@acipere.com>

# add unstable repo and set pins
RUN echo "deb http://ftp.us.debian.org/debian unstable main" >> /etc/apt/sources.list \
	&& echo "Package: *\nPin: release a=unstable\nPin-Priority: 800" >> /etc/apt/preferences

RUN apt update \
        && apt -y install bitcoin-qt wget \
        && apt -y upgrade \
        && apt clean \
        && rm -rf /var/lib/apt/lists/*

RUN wget https://www.bitcoinunlimited.info/downloads/BUcash-1.1.0.0-linux64.tar.gz \
        && tar -xvf BUcash-1.1.0.0-linux64.tar.gz \
        && cp -Rv /BUcash-1.1.0/* /usr/ \
        && rm -rf /BUcash*

# Add user
RUN groupadd -g 11225 -r bcc && useradd -u 11120 -r -G video -g bcc bcc \
    && mkdir -p /home/bcc/.bitcoin/blocks \
    && mkdir -p /home/bcc/.bitcoin/chainstate \
    && chown -Rv bcc:bcc /home/bcc

USER bcc

CMD ["/bin/bitcoin-qt"]


